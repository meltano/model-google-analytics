{
  include "mixins/report_date.m5o"

  version = 1
  sql_table_name = google_analytics_locations
  name = google_analytics_locations
  columns {
    report_year {
      label = Year
      description = Year
      type = string
      sql = "{{table}}.report_year"
    }
    report_month {
      label = Month
      description = Month
      type = string
      sql = "{{table}}.report_month"
    }
    report_day {
      label = Day
      description = Day
      type = string
      sql = "{{table}}.report_day"
    }
    continent {
      primary_key = true
      hidden = false
      label = Continent
      description = Continent
      type = string
      sql = "{{table}}.continent"
    }
    sub_continent {
      primary_key = true
      hidden = false
      label = Sub-Continent
      description = Sub-Continent
      type = string
      sql = "{{table}}.sub_continent"
    }
    country {
      primary_key = true
      hidden = false
      label = Country
      description = Country
      type = string
      sql = "{{table}}.country"
    }
    region {
      primary_key = true
      hidden = false
      label = Region
      description = Region
      type = string
      sql = "{{table}}.region"
    }
    metro {
      primary_key = true
      hidden = false
      label = Metro
      description = Metro
      type = string
      sql = "{{table}}.metro"
    }
    city {
      primary_key = true
      hidden = false
      label = City
      description = City
      type = string
      sql = "{{table}}.city"
    }
  }
  aggregates {
    total_users {
      label = Total Users
      description = Total Users
      type = sum
      sql = "{{table}}.users"
    }
    total_new_users {
      label = Total New Users
      description = Total New Users
      type = sum
      sql = "{{table}}.new_users"
    }
    total_sessions {
      label = Total Sessions
      description = Total Sessions
      type = sum
      sql = "{{table}}.sessions"
    }
    avg_sessions_per_user {
      label = Average Sessions Per User
      description = Average Sessions Per User
      type = avg
      sql = "{{table}}.sessions_per_user"
    }
    avg_session_duration {
      label = Average Session Duration
      description = Average Session Duration
      type = avg
      sql = "{{table}}.avg_session_duration"
    }
    total_pageviews {
      label = Total Pageviews
      description = Total Pageviews
      type = sum
      sql = "{{table}}.pageviews"
    }
    avg_pageviews_per_session {
      label = Average Pageviews Per Session
      description = Average Pageviews Per Session
      type = avg
      sql = "{{table}}.pageviews_per_session"
    }
    avg_time_on_page {
      label = Average Time On Page (sec)
      description = Average Time On Page (sec)
      type = avg
      sql = "{{table}}.avg_time_on_page"
    }
    avg_bounce_rate {
      label = Average Bounce Rate (%)
      description = Average Bounce Rate (%)
      type = avg
      sql = "{{table}}.bounce_rate"
    }
    avg_exit_rate {
      label = Average Exit Rate (%)
      description = Average Exit Rate (%)
      type = avg
      sql = "{{table}}.exit_rate"
    }
  }
}
