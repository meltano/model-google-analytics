# model-google-analytics

Meltano .m5o models for data fetched from the [Google Analytics Reporting API](https://developers.google.com/analytics/devguides/reporting/core/v4/) using [tap-google-analytics](https://gitlab.com/meltano/tap-google-analytics).